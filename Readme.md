# Шаблон приложения Blazor с интерактивным auto-рендерингом

# Задачи

* Добавить примеры с загрузкой данных из БД, показывая в это время призрак
* Добавить создание root учетной записи
* Взять страницы Identity с проекта 
https://github.com/fullstackhero/blazor-starter-kit?tab=readme-ov-file 
* Подсмотреть структуру и другие примеры с того же проекта
* Добавить авторизацию через vk, yandex, google, ...
* Добавить интеграцию с платежным сервисом (например https://robokassa.com/ https://yookassa.ru/) 

* тестануть логирование
* Тестануть менеджмент ролей через Roles.Admin (например)
* Добавить админ панель с табличкой пользователей 


## Рассматриваем проект blazor-starter-kit. Что хочется взять:
* DatabaseSeeder, UserService, AccountService, TokenService, CurrentUserService