﻿namespace BlazorApp_V1.Shared
{
    public class AntiforgeryDefaults
    {
        public const string HeaderName = "X-XSRF-TOKEN";
        public const string CookieName = "__Host-X-XSRF-TOKEN";
    }
    public static class AuthDefaults
    {
        public const string AuthorizedClientName = "authorizedClient";

        public const string LogInPath = "LogInPath";
        public const string LogOutPath = "LogOutPath";
    }


    public class WeatherForecast
    {
        public DateOnly Date { get; set; }
        public int TemperatureC { get; set; }
        public string? Summary { get; set; }
        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);
    }

    public class UserInfo
    {
        public static readonly UserInfo Anonymous = new();

        public bool IsAuthenticated { get; set; }

        public string NameClaimType { get; set; } = string.Empty;

        public string RoleClaimType { get; set; } = string.Empty;

        public ICollection<ClaimValue> Claims { get; set; } = new List<ClaimValue>();
    }

    public class ClaimValue
    {
        public ClaimValue()
        {
        }

        public ClaimValue(string type, string value)
        {
            Type = type;
            Value = value;
        }

        public string Type { get; set; } = string.Empty;

        public string Value { get; set; } = string.Empty;
    }
}
