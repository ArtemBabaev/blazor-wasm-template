﻿using BlazorApp_V1.Shared;
using Microsoft.JSInterop;
using System.Net.Http;

namespace Casdoor.BlazorOidcExample.Client.Services
{

    public interface IAntiforgeryHttpClientFactory
    {
        Task<HttpClient> CreateClientAsync(string clientName = AuthDefaults.AuthorizedClientName);
    }
    public class AntiforgeryHttpClientFactory : IAntiforgeryHttpClientFactory
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IJSRuntime _js;

        public AntiforgeryHttpClientFactory(IHttpClientFactory httpClientFactory, IJSRuntime jSRuntime)
        {
            _httpClientFactory = httpClientFactory;
            _js = jSRuntime;
        }

        public async Task<HttpClient> CreateClientAsync(string clientName = AuthDefaults.AuthorizedClientName)
        {
            var token = await _js.InvokeAsync<string>("getAntiForgeryToken");
            Console.WriteLine($"Token: {token}");

            var client = _httpClientFactory.CreateClient(clientName);
            client.DefaultRequestHeaders.Add(AntiforgeryDefaults.HeaderName, token);

            return client;
        }
    }
}