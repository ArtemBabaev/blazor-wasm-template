var builder = DistributedApplication.CreateBuilder(args);

builder.AddProject<Projects.BlazorApp_V1>("blazorapp-v1");

builder.Build().Run();
